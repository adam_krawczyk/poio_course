## POIO 2020 source codes

### Run code

To run home cw1/2 execute following commands:

```
git clone https://gitlab.com/adam_krawczyk/poio_course.git
cd poio_course/home_lab1/
mkdir build && cd build && cmake ..
make

```

`./../bin/cw_1` or `./../bin/cw_2`




## Project part

This part is response to following task:

[task](https://documentcloud.adobe.com/link/review/?pageNum=1&uri=urn%3Aaaid%3Ascds%3AUS%3Ae3370309-aaae-40b5-a02c-ed61d8653cdf)

## Make 

### REMOVED 

        To make tests first install gtest 

        ```bash
        sudo apt-get install libgtest-dev
        sudo apt-get install cmake # install cmake
        cd /usr/src/gtest
        sudo cmake CMakeLists.txt
        sudo make
        
        # copy or symlink libgtest.a and libgtest_main.a to your /usr/lib folder
        sudo cp *.a /usr/lib
        ```

Then you can compile following way

```
cd typy_danych/build
cmake ..
make 
./../bin/main
```


## Vectors

To run task:

```
cd vectors/src
g++ -o main main.cxx && ./main
```
