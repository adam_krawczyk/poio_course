#include <iostream>
using namespace std;

#include <vector>
#include <time.h>

int main()
{

    int tab_int[5];
    int tab_int_2[] = {1, 3, 6, 7};
    int tab_int_3[5] = {};

    for (int i = 0; i < 5; i++)
        cout << "tab_int[" << i << "] = " << tab_int[i] << endl;
    cout << endl;

    for (int i = 0; i < 5; i++)
        cout << "tab_int_2[" << i << "] = " << tab_int_2[i] << endl;
    cout << endl;

    for (int i = 0; i < 5; i++)
        cout << "tab_int_3[" << i << "] = " << tab_int_3[i] << endl;
    cout << endl;

    /* vector */

    vector<int> v1(5);
    vector<int> v2 = {1, 3, 6, 7};
    vector<int> v3; //wskaznik zero

    //metody vector
    for (unsigned int i = 0; i < v1.size(); i++)
        cout << "v1[" << i << "] = " << v1[i] << endl;
    cout << "v1 max size = " << v1.max_size() << endl;
    cout << endl;

    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    for (unsigned int i = 0; i < v3.size(); i++)
        cout << "v3[" << i << "] = " << v3[i] << endl;
    cout << "v3 max size = " << v3.max_size() << endl;
    cout << endl; 

    cout << "v1 size = " << v1.size() << endl;
    cout << "v2 size = " << v2.size() << endl;
    cout << "v3 size = " << v3.size() << endl;
    cout << endl;

    cout << "first el. v2 = " << v2.front() << endl;
    cout << "last el. v2 = " << v2.back() << endl;

    cout << "size of vector 2 before push: " << v2.size() << endl;
    cout << "push" << endl;
    v2.push_back(12);
    v2.push_back(2);
    v2.push_back(6);
    v2.push_back(4);

    cout << "size of vector 2: " << v2.size() << endl;
    cout << "elements of vector 2: " << endl;
    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    cout << "pop" << endl;

    v2.pop_back();
    v2.pop_back();
    v2.pop_back();
    cout << "size of vector 2 po pop: " << v2.size() << endl;
    cout << "elements of vector 2: " << endl;
    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    cout << "resize down" << endl;
    v2.resize(2);
    cout << "size of vector 2: " << v2.size() << endl;
    cout << "elements of vector 2: " << endl;
    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    cout << "resize down" << endl;
    v2.resize(5);
    cout << "size of vector 2: " << v2.size() << endl;
    cout << "elements of vector 2: " << endl;
    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    cout << "clear" << endl;
    v2.clear();
    cout << "size of vector 2: " << v2.size() << endl;
    cout << "elements of vector 2: " << endl;
    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    srand(time(NULL));
    for (int i = 0; i < 7; i++)
        v2.push_back(rand() % 100);
    cout << "size of vector 2: " << v2.size() << endl;
    cout << "elements of vector 2: " << endl;
    for (unsigned int i = 0; i < v2.size(); i++)
        cout << "v2[" << i << "] = " << v2[i] << endl;
    cout << "v2 max size = " << v2.max_size() << endl;
    cout << endl;

    for (int el : v2) //el ktry naley do v2 i mona wyswietlac te elements of
        cout << el << ", ";
    cout << endl;
    cout << endl;
    cout << endl;

    /* iterators  */

    cout << "iterators" << endl;

    vector<int>::iterator it1 = v2.begin() + 2;
    cout << "third from begining el v2: " << *it1 << endl;
    it1 = v2.end() - 1; //bo end wskazuje na pamiec za wektorem w ktorej mozna przypisac kolejna komorke
    cout << "last el v2: " << *it1 << endl;
    it1 = v2.begin() + v2.size() - 1;
    cout << "last el v2: " << *it1 << endl;

    cout << "Adres it1 =" << it1.base() << endl;

    vector<int>::reverse_iterator rit1 = v2.rbegin() + 2;
    cout << "third from end el v2: " << *rit1 << endl;
    rit1 = v2.rend() - 1;
    cout << "first el v2: " << *rit1 << endl;

    cout << endl;
    cout << "el 2 vector" << endl;
    for (int el : v2)
        cout << el << ", ";
    cout << endl;
    cout << "erase(1)" << endl;
    v2.erase(v2.begin() + 1);
    cout << "el 2 vector" << endl;
    for (int el : v2)
        cout << el << ", ";
    cout << endl;
    cout << endl;

    cout << "erase(0, 2)" << endl;
    v2.erase(v2.begin(), v2.begin() + 1);
    cout << "el 2 vector" << endl;
    for (int el : v2)
        cout << el << ", ";
    cout << endl;

    cout << "Insert 1" << endl;
    v2.insert(v2.begin() + 2, 3, 7); 
    cout << "el 2 vector" << endl;
    for (int el : v2)
        cout << el << ", ";
    cout << endl;

    v2.insert(v2.end() - 2, 5, {});
    cout << "el 2 vector" << endl;
    for (int el : v2)
        cout << el << ", ";
    cout << endl;

    return 0;
}
