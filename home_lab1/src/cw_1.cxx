#include <iostream>
#include <vector>
#include <sstream>

#include "Complex.hpp"

using namespace std;

// variadic template
template <typename... Args>
std::string sstr(Args &&... args)
{
    std::ostringstream sstr;
    // fold expression
    (sstr << std::dec << ... << args);
    return sstr.str();
}

int main()
{

    /* Zadanie 1 p 1 */
    //jawne wywołanie konstruktorów
    Complex jc1{};
    Complex jc2{"c2-name", 5.756, 32.2};
    Complex jc3{3.5353, 9676};
    auto jc4{jc2};

    // niejawne wywołanie
    auto nc1 = new Complex{};
    Complex *nc2 = new (Complex){534.5, 242.5};
    Complex *nc3 = static_cast<Complex *>(new Complex{"nc3-name", 434.2, -12313.3});
    auto nc4 = nc2;

    //infos
    jc1.info();
    jc2.info();
    jc3.info();
    jc4.info();

    nc1->info();
    nc2->info();
    nc3->info();
    nc4->info();

    nc1->~Complex();
    nc2->~Complex();
    nc3->~Complex();
    nc4->~Complex();

    /* Zadanie 1 p 2 */
    vector<Complex> complex_vector; //should be on tables but vectors are more cool ;)

    for (int i = 0; i < 3; i++)
    {
        complex_vector.push_back(Complex{"for-name " + sstr(i), i, i});
    }

    for (auto i : complex_vector)
    {
        i.info();
    }

    /* Zadanie 1 p 3 */
    auto c1 = Complex{"num1", 10.5, 3};
    auto c2 = Complex{"num2", 3, 90.5};

    auto sum = c1+c2;
    auto sub = c1-c2;
    auto mut = c1*c2;
    auto div = c1/c2;

    sum.setName("Sum of two complex numbers");
    sub.setName("Subtraction of two complex numbers");
    mut.setName("Multiply of two complex numbers");
    div.setName("Divide of two complex numbers");

    sum.info();
    sub.info();
    mut.info();
    div.info();

    /* Zadanie 1 p 4 */
    if (c1 > c2)
    {
        cout << "c1 jest większa." << endl;
    }
    else
    {
        cout << "c2 jest większa." << endl;
    }

    if (c1 < c2)
    {
        cout << "c1 jest mniejsza." << endl;
    }
    else
    {
        cout << "c2 jest mniejsza." << endl;
    }

    if (c1 >= c2)
    {
        cout << "c1 jest większa lub równa." << endl;
    }
    else
    {
        cout << "c2 jest większa lub równa." << endl;
    }

    if (c1 <= c2)
    {
        cout << "c1 jest mniejsza lub równa." << endl;
    }
    else
    {
        cout << "c2 jest mniejsza lub równa." << endl;
    }

    if (c1 == c2)
    {
        cout << "liczby są równe." << endl;
    }
    else
    {
        cout << "liczby są nie równe." << endl;
    }

    if (c1 != c2)
    {
        cout << "liczby są nie równe." << endl;
    }
    else
    {
        cout << "liczby są równe." << endl;
    }

    return 0;
}