#include "min_max.hpp"
#include <iostream>
#include "Complex.hpp"
#include "Sort.hpp"
#include <random>

using namespace std;

int main()
{

    auto c1 = Complex(2, 5);
    auto c2 = Complex(1, 1);
    auto c3 = Complex(6, 7);

    cout << "max z 4: " << maximum(4, 24, 2, 1) << endl;
    cout << "max z 3: " << maximum(4, 34, 2) << endl;
    cout << "max z 2: " << maximum(4523, 24) << endl;

    cout << "max z 2 COMPLEX   : " << maximum(c1, c2) << endl;


    //minimum

    cout << "min z 4: " << minimum(4, 24, 2, 1) << endl;
    cout << "min z 3: " << minimum(4, 24, 2) << endl;
    cout << "min z 2: " << minimum(4, 24) << endl;
    cout << "min z 2 COMPLEX   : " << minimum(c1, c2) << endl;


    //template
    Sort<int, 5> tab_int{};
    int index = 0;
    // while(tab_int.set(index++,0));
    cout << index << endl;

    tab_int.printValues();

    Sort<double> tab_double{};
    index = 0;
    while (tab_double.set(index++, rand() % 3 + rand() % 100))
        ;
    tab_double.printValues();
    cout << "sorted: " << endl;
    tab_double.bubble_sort_values();
    tab_double.printValues();

    Sort<Complex> tab_complex;
    index = 0;
    while (tab_complex.set(index++, Complex(rand() % 100, (rand() % 743))))
        ;

    tab_complex.printValues();

    cout << "sorted: " << endl;
    tab_complex.bubble_sort_values();
    tab_complex.printValues();
}