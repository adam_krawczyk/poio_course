#include <iostream>
#include <iomanip> // std::setw

#include "Complex.hpp"

using namespace std;

void showMenu();
void showResult(Complex);

int main()
{
    Complex c1, c2, result;
    int menu;

    showMenu();

    c1.inputFromConsole();
    c2.inputFromConsole();

    std::cin >> menu;

    switch (menu)
    {
    case 1:
        result = c1 + c2;
        break;
    case 2:
        result = c1 - c2;
        break;
    case 3:
        result = c1 * c2;
        break;
    case 4:
        result = c1 / c2;
        break;
    case 5:
        result = c1.reciprocal();
        break;
    case 6:
        result = c2.reciprocal();
        break;
    case 7:
        result = c1.conj();
        break;
    case 8:
        result = c2.conj();
        break;

    default:
        cout << "unrecognized option" << endl
             << "options are 1 - add, 2 - subtract, 3 - multiply, 4 - divide, 5 - num 1 inverse,"
             << " 6 - num 2 inverse, 7 - num 1 conjugate, 8 - num 2 conjugate " << endl;
        break;
    }

    showResult(result);

    return 0;
}

void showMenu()
{
    cout << "Welcome in calculator!!!" << endl
         << "First define numbers then set operation you want to perform"
         << "Aviable functions in complex number calculator: " << endl
         << setw(5) << "|"
         << "add"
         << "|"
         << "subtract"
         << "|"
         << "multiply"
         << "|"
         << "divide"
         << "|" << endl
         << setw(5) << "|"
         << "inverse 1 num"
         << "|"
         << "inverse 2 num"
         << "|"
         << "conjugate 1 num"
         << "|"
         << "conjugate 2 num"
         << "|" << endl
         << setw(5) << "Please enter option you want to perform 1-8" << endl;
}

void showResult(Complex data)
{
    cout << "Wynik to:" << endl
         << "Real part: " << data.getReal() << endl
         << " Imaginary part: " << data.getImag() << endl;
}