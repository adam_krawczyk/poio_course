#include <iostream>
#include "Complex.hpp"

using namespace std;

int main()
{

    Complex *c1 = new Complex();

    c1->info();

    c1->setReal(4.5);
    c1->setImaginary(2);

    c1->info();

    auto c2 = Complex();
    c2.inputFromConsole();
    c2.info();
    auto c_add = c1->operator+(c2);
    auto c_subtract = c1->operator-(c2);

    auto c3 = c2.conj();
    c3.conj();

    c1->~Complex();
    return 0;
}