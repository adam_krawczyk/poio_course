#include "Complex.hpp"

//using namespace cmplx;

Complex::Complex(/* args */)
{
    this->name = "no_name!";
    this->imag = 0.0;
    this->real = 0.0;
    this->recalculateData();
}

Complex::Complex(std::string name, double real_value, double imaginary_value)
{
    this->real = real_value;
    this->imag = imaginary_value;
    this->name = name;
    this->recalculateData();
}

Complex::Complex(double real_value, double imaginary_value) : real(real_value), imag(imaginary_value)
{
    this->recalculateData();
}

Complex::Complex(const Complex &n)
{
    this->arg = n.arg;
    this->mod = n.mod;
    this->imag = n.imag;
    this->real = n.real;
    this->name = n.name;
    this->recalculateData();
}

Complex::~Complex()
{
    std::cout << "destroing object: " << this << " name: " << this->name << std::endl;
}

//private methods
void Complex::calculateModule()
{
    this->mod = pow((pow(this->real, 2) * pow(this->imag, 2)), 0.5);
}

void Complex::calculateArgument()
{
    this->arg = atan2(this->real, this->imag);
}

void Complex::recalculateData()
{
    this->calculateArgument();
    this->calculateModule();
}

//operators
Complex Complex::operator+(const Complex &r) const
{
    Complex res;
    res.real = this->real + r.real;
    res.imag = this->imag + r.imag;
    return res;
}

Complex Complex::operator-(const Complex &r) const
{
    Complex res;
    res.real = this->real - r.real;
    res.imag = this->imag - r.imag;
    return res;
}

Complex Complex::operator*(const Complex &r) const
{
    Complex res;
    res.real = ((this->real * r.real)-(this->imag * r.imag));
    res.imag = ((this->imag * r.imag)+(this->imag * r.imag));
    return res;
}

Complex Complex::operator/(const Complex &r) const
{
    Complex res;
    res.real = this->real / r.real;
    res.imag = this->imag / r.imag;
    return res;
}

std::ostream & operator<<(std::ostream &console_out, const Complex &number){
return console_out<<number.real<<"+"<<number.imag<<"i";
}

//public methods
void Complex::info()
{
    std::cout << "name: " << this->name << std::endl;
    std::cout << "real: " << this->real << std::endl;
    std::cout << "imaginary: " << this->imag << std::endl;
    std::cout << "module: " << this->mod << std::endl;
    std::cout << "argument: " << this->arg << std::endl;
}

void Complex::setName(std::string name)
{
    this->name = name;
}

void Complex::setImaginary(double im)
{
    this->imag = im;
    this->recalculateData();
}

void Complex::setReal(double re)
{
    this->real = re;
    this->recalculateData();
}

void Complex::inputFromConsole()
{
    try
    {
        std::cout << "Insert name for complex number" << std::endl;
        std::cin >> this->name;
        std::cout << "Insert real part for complex number" << std::endl;
        std::cin >> this->real;
        std::cout << "Insert imaginary part for complex number" << std::endl;
        std::cin >> this->imag;
        this->recalculateData();
    }
    catch (...)
    {
        std::cout << "wrong data input" << std::endl;
    }
}

std::string Complex::getName()
{
    return this->name;
}

double Complex::getReal()
{
    return this->real;
}

double Complex::getImag()
{
    return this->imag;
}

Complex Complex::reciprocal()
{
    Complex tmp;
    tmp.real = ((this->real) / abs(pow(this->real,2) + pow(this->imag,2)));
    tmp.imag = ((this->imag) / abs(pow(this->real,2) + pow(this->imag,2)));
    return tmp;
}
Complex Complex::conj()
{
    Complex tmp;
    tmp.real = this->real;
    tmp.imag = (this->imag * (-1));
    return tmp;
}


// friend fcn
bool operator>(Complex const &l, Complex const &r)
{
    return (l.real > r.real) ? true : false;
}

bool operator<(Complex const &l, Complex const &r)
{
    return (l.real < r.real) ? true : false;
}
bool operator<=(Complex const &l, Complex const &r)
{
    return (l.real >= r.real) ? true : false;
}
bool operator>=(Complex const &l, Complex const &r)
{
    return (l.real <= r.real) ? true : false;
}
bool operator==(Complex const &l, Complex const &r)
{
    return (l.real == r.real) ? true : false;
}
bool operator!=(Complex const &l, Complex const &r)
{
    return (l.real != r.real) ? true : false;
}