#ifndef MIN_MAX_HPP
#define MIN_MAX_HPP

template <typename T>
T maximum(T number_1, T number_2)
{
    return number_1 > number_2 ? number_1 : number_2;
}

template <typename T>
T minimum(T number_1, T number_2)
{
    return number_1 < number_2 ? number_1 : number_2;
}

template <typename T, typename ... Args>
T maximum(T number_1, T number_2, Args ... args)
{
    return maximum(maximum(number_1, number_2),args ...);
}

template <typename T, typename ... Args>
T minimum(T number_1, T number_2, Args ... args)
{
    return minimum(minimum(number_1, number_2),args ...);
}

template <typename T>
T maximum_of_3(T n1, T n2, T n3)
{
    if (n1 > n2)
    {
        if (n1 > n3)
        {
            return n1;
        }
        if (n1 < n3)
        {
            return n3;
        }
    }
    else if (n2 > n3)
    {
        return n2;
    }
    else
    {
        return n3;
    }
    
    

}

template <typename T>
T minimum_of_3(T n1, T n2, T n3)
{
    if (n1 < n2)
    {
        if (n1 < n3)
        {
            return n1;
        }
        if (n1 > n3)
        {
            return n3;
        }
    }
    else if (n2 < n3)
    {
        return n2;
    }
    else
    {
        return n3;
    }
    
    

}


// Dopisac min dla 3 i dla dowolnej ilosci arg

#endif



