#ifndef SORT_HPP
#define SORT_HPP
#include <iostream>

using namespace std;

template <typename T, int I = 10>
class Sort
{
private:
    T table[I];

public:
    bool set(int index, const T t)
    {
        if (index < I and index >= 0)
        {
            table[index] = t;
            return true;
        }
        else
        {
            return false;
        }
    }

    void bubble_sort_values()
    {
        int i{};
        T temp;
        bool was_change{};

        do
        {
            was_change = false;
            for (i = 0; i < (I - i); i++)
            {
                if (table[i] > table[i + 1])
                {
                    temp = table[i];
                    table[i] = table[i + 1];
                    table[i + 1] = temp;
                }
            }
        }while (was_change);
        
    }

    void printValues()
    {
        for (int i = 0; i < I - 1; i++)
        {
            cout << table[i] << ", ";
        }
        cout << table[I - 1] << "." << endl;
    }
};

#endif