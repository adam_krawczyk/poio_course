#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <string>
#include <iostream>
#include <cmath>

//namespace cmplx{
class Complex
{
private:
    std::string name{};
    double real{}, imag{}, mod{}, arg{};

    virtual void calculateModule();
    virtual void calculateArgument();
    virtual void recalculateData();

public:
    Complex();
    Complex(std::string name, double real_part, double imaginary_part);
    Complex(double real_part, double imaginary_part);
    Complex(const Complex &number);

    ~Complex();

    Complex operator+(Complex const &r)const;
    Complex operator-(Complex const &r)const;
    Complex operator*(Complex const &r)const;
    Complex operator/(Complex const &r)const;
    
    friend bool operator>(Complex const &, Complex const &); //actually there is no way to compare complec num in different fields than == / !=
    friend bool operator<(Complex const &, Complex const &);
    friend bool operator<=(Complex const &, Complex const &);
    friend bool operator>=(Complex const &, Complex const &);
    friend bool operator==(Complex const &, Complex const &);
    friend bool operator!=(Complex const &, Complex const &); 

    friend std::ostream & operator<<(std::ostream &console_out, const Complex &number);

    void info();
    Complex reciprocal();
    Complex conj();

    //setters
    void setName(std::string name);
    void setImaginary(double im);
    void setReal(double re);
    void inputFromConsole();

    //getters
    std::string getName();
    double getReal();
    double getImag();
}; //class

//} //namespace cplx



#endif //COMPLEX_H_