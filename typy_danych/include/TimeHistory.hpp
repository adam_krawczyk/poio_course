#ifndef TIMEHISTORY_HPP
#define TIMEHISTORY_HPP

#include "Sequence.hpp"
#include <iostream>

template<typename T>
class TimeHistory : public Sequence<T>
{
public:
    TimeHistory() //: Sequence()
    {
    }
    TimeHistory(std::string _name, std::string _description, intmax_t _time, double _sensitivity, uint _chanel_nr, std::string _unit, double _res)
    {
        this->device = {_name};
        this->description = {_description};
        this->date = {_time};
        this->chanelNr = {_chanel_nr};
        this->unit = {_unit};
        this->resolution = {_res};
    }
    ~TimeHistory(){};
    void printData()
    {
        std::cout << "Data inside Time History!!" << std::endl
                  << "Sending device name: " << this->device << std::endl
                  << "Time History description: " << this->description << std::endl
                  << "Time History timestamp: " << this->date << std::endl
                  << "Time History chanel used: " << this->chanelNr << std::endl
                  << "Time History resolution: " << this->resolution << std::endl
                  << "Time History unit: " << this->unit << std::endl
                  << "Time History sensitivity: " << this->sensitivity << std::endl
                  << std::endl;
    }
    void loadRandomData()
    {
        this->sensitivity = rand() / 3;

        //sequence data
        this->chanelNr = rand();
        this->unit = "TH Unit" + this->generateRandomString(3);
        this->resolution = rand() / 3;

        //packet data
        this->device = "TH Device" + this->generateRandomString(2);
        this->description = "TH Description" + this->generateRandomString(5);
        this->date = rand();
    }

    std::string toString()
    {
        std::string s =
            "Data inside TimeHistory!! \n Sending device name: " + static_cast<std::string>(this->device) +
            "\n TimeHistory description: " + static_cast<std::string>(this->description) +
            "\n TimeHistory timestamp: " + std::to_string(this->date) +
            "\n TimeHistory sensitivity: " + std::to_string(this->sensitivity) +
            "\n TimeHistory chanelNr: " + std::to_string(this->chanelNr) +
            "\n TimeHistory unit: " + static_cast<std::string>(this->unit) +
            "\n TimeHistory resolution: " + std::to_string(this->resolution);

        return s;
    }

private:
    double sensitivity{};
};

#endif