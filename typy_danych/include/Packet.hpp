#ifndef PACKET_HPP
#define PACKET_HPP

#include <string>
#include <cstdlib>
#include <random>

class Packet
{
protected:
    std::string device{};
    std::string description{};
    intmax_t date{};
    std::string generateRandomString(std::string::size_type length);

public:
    virtual void printData() = 0; // for testing
    virtual void loadRandomData() = 0;
    virtual std::string toString() = 0;
};

#endif