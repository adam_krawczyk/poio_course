#ifndef SPECTRUM_HPP
#define SPECTRUM_HPP

#include "Sequence.hpp"

template <typename T>
class Spectrum : public Sequence<T>
{
public:
    Spectrum()
    {
    }
    Spectrum(std::string _name, std::string _description, intmax_t _date, bool _scaling, uint32_t _chanel_nr, std::string _unit, double _resolution)
    {
        this->device = {_name};
        this->description = {_description};
        this->date = {_date};
        this->scaling = {_scaling};
        this->chanelNr = {_chanel_nr};
        this->unit = {_unit};
        this->resolution = {_resolution};
    }
    ~Spectrum(){};

    void loadRandomData()
    {
        this->scaling = rand() % 2;

        //sequence data
        this->chanelNr = rand();
        this->unit = "SP Unit" + this->generateRandomString(3);
        this->resolution = rand() / 3;

        //packet data
        this->device = "SP Device" + this->generateRandomString(2);
        this->description = "SP Description" + this->generateRandomString(5);
        this->date = rand();
    };
    std::string toString()
    {
        std::string s =
            "Data inside Spectrum!! \n Sending device name: " + static_cast<std::string>(this->device) +
            "\n Spectrum description: " + static_cast<std::string>(this->description) +
            "\n Spectrum timestamp: " + std::to_string(this->date) +
            "\n Spectrum scaling: " + std::to_string(this->scaling) +
            "\n Spectrum chanelNr: " + std::to_string(this->chanelNr) +
            "\n Spectrum unit: " + static_cast<std::string>(this->unit) +
            "\n Spectrum resolution: " + std::to_string(this->resolution);

        return s;
    }

private:
    bool scaling{};
};

#endif