#ifndef SEQUENCE_HPP
#define SEQUENCE_HPP

#include "Packet.hpp"
#include <iostream>
#include <string>
#include <vector>

template <typename T>
class Sequence : public Packet
{
public:
    virtual void loadRandomData() { std::cerr << "This method should be called inside specified class" << std::endl; }
    virtual std::string toString() { std::cerr << "This method should be called inside specified class" << std::endl; return "e";}
    virtual void printData()
    {
        std::cout << "Data inside Sequence!!" << std::endl
                  << "Sending device name: " << this->device << std::endl
                  << "Sequence description: " << this->description << std::endl
                  << "Sequence timestamp: " << this->date << std::endl
                  << "Sequence chanel used: " << this->chanelNr << std::endl
                  << "Sequence resolution: " << this->resolution << std::endl
                  << "Sequence unit: " << this->unit << std::endl
                  << std::endl;
    }

    T& operator[](int i){
        if((i<0 or (i>= buffer.size()))){
            throw i;
        }
        return buffer_old[i];
    }

protected:
    T buffer_old[1024];
    std::vector<T> buffer;

    uint32_t chanelNr{};

    std::string unit{};

    double resolution{};

    Sequence(){};

    virtual ~Sequence(){};
};

#endif
