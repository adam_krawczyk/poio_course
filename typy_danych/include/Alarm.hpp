#ifndef ALARM_HPP
#define ALARM_HPP

#include "Packet.hpp"
#include <iostream>

template <typename T>
class Alarm : public Packet
{
public:
    Alarm()
    {
    }
    Alarm(std::string _name, std::string _desctiption, intmax_t _date, double _threshold, uint32_t _chanel_nr, bool _direction)
    {
        this->device = {_name};
        this->description = {_desctiption};
        this->date = {_date};
        this->threshold = {_threshold};
        this->chanelNr = {_chanel_nr};
        this->direction = {_direction};
    }

    ~Alarm()
    {
    }

    virtual void printData()
    {
        std::cout << "Data inside Alarm!!" << std::endl
                  << "Sending device name: " << this->device << std::endl
                  << "Alarm description: " << this->description << std::endl
                  << "Alarm timestamp: " << this->date << std::endl
                  << "Alarm chanel used: " << this->chanelNr << std::endl
                  << "Alarm threshold: " << this->threshold << std::endl
                  << "Alarm direction: " << this->direction << std::endl
                  << std::endl;
    }
    void loadRandomData()
    {
        this->chanelNr = rand();
        this->threshold = rand() / 3;
        this->direction = rand() % 2;

        //packet data
        this->device = "AL Device" + this->generateRandomString(2);
        this->description = "AL Description" + this->generateRandomString(5);
        this->date = rand();
    }
    std::string toString()
    {
        std::string s =
            "Data inside Alarm!! \n Sending device name: " + static_cast<std::string>(this->device) +
            "\n Alarm description: " + static_cast<std::string>(this->description) +
            "\n Alarm timestamp: " + std::to_string(this->date) +
            "\n Alarm threshold: " + std::to_string(this->threshold) +
            "\n Alarm chanelNr: " + std::to_string(this->chanelNr) +
            "\n Alarm direction: " + std::to_string(this->direction);

        return s;
    }

private:
    /* data */
    uint32_t chanelNr{};
    double threshold{};
    bool direction{};
};

#endif