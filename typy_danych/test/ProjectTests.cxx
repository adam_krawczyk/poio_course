#include <gtest/gtest.h>
#include <../include/TimeHistory.hpp>
#include <../include/Spectrum.hpp>
#include <../include/Alarm.hpp>

struct ProjectTests : public ::testing::Test
{
    TimeHistory th{};
    Alarm a{};
    Spectrum s{};

    virtual void SetUp() override
    {
        printf("setup!!!!!!!!!\n");
    }
    virtual void TearDown()
    {
        printf("closing!!!!!!!\n");
    }
};

TEST_F(ProjectTests, TestType)
{
    EXPECT_EQ(typeid(Spectrum).name(), typeid(s).name()) << "Failed value don't match expected: " << typeid(Spectrum).name() << " real value is: " << typeid(s).name() << "\n";
    EXPECT_EQ(typeid(Alarm).name(), typeid(a).name()) << "Failed value don't match expected: " << typeid(Alarm).name() << " real value is: " << typeid(a).name() << "\n";
    EXPECT_EQ(typeid(TimeHistory).name(), typeid(th).name()) << "Failed value don't match expected: " << typeid(TimeHistory).name() << " real value is: " << typeid(th).name() << "\n";
}

TEST_F(ProjectTests, TestRetVal)
{
    EXPECT_EQ(typeid(std::string).name(), typeid(s.toString()).name());
    EXPECT_EQ(typeid(std::string).name(), typeid(a.toString()).name());
    EXPECT_EQ(typeid(std::string).name(), typeid(th.toString()).name());

}
// expect -> continue testing
// assert -> breaks