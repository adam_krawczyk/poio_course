#include <iostream>
#include <memory>
#include "Spectrum.hpp"
#include "Alarm.hpp"
#include "TimeHistory.hpp"


using namespace std;

int main()
{
    cout << "Working" << endl;

    auto s = Spectrum<double>();
    auto a = Alarm<double>();
    auto t = TimeHistory<double>();
    // Sequence<int> s1; //imposible because constructor is protected

    Packet *p_ptr[3] = {&s, &a, &t};
    for (int nr = 0; nr < 3; nr++)
    {
        p_ptr[nr]->printData();
    }

    cout << "Filling with random data and printing: " << endl;
    for (int nr = 0; nr < 3; nr++)
    {
        p_ptr[nr]->loadRandomData();
        auto s = p_ptr[nr]->toString();
        cout << endl
             << s << endl;
    }

    std::unique_ptr<Spectrum<double>> sptr(new Spectrum<double>("s_name", "s_des", 123400, 1, 18, "[N]", 1080));
    std::unique_ptr<Alarm<int>> aptr(new Alarm<int>("a_name", "a_des", 8972384712983472, 0.00023412234, 19, 0));
    std::unique_ptr<TimeHistory<float>> thptr(new TimeHistory<float>("th_name", "th_des", 76234178364127, 8234293.234894, 20, "[m/s]", 720));

    auto spectrum_string = sptr->toString();
    auto alarm_string = aptr->toString();
    auto time_history_string = thptr->toString();

    cout<<endl<<"printing specialized objects data:"<<endl;
    cout<<endl<<spectrum_string<<endl<<endl<<alarm_string<<endl<<endl<<time_history_string<<endl;

    cout<<s[5]<<endl;
    
    sptr.reset();
    aptr.reset();
    thptr.reset();

}