set(This Project)
project(${This})
cmake_minimum_required(VERSION 3.8)

set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/bin)
set(SOURCE_DIR ${CMAKE_SOURCE_DIR}/src)
set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})


set(CMAKE_C_STANDARD 99)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include_directories(include)

#enable_testing()

# add_subdirectory(googletest) installed globally you can clone repo here and uncomment this 


set(Headers
    include/Spectrum.hpp
    include/Alarm.hpp
    include/Packet.hpp
    include/Sequence.hpp
    include/TimeHistory.hpp
)

set(Sources
#    src/Spectrum.cxx
#    src/Alarm.cxx
    src/main.cxx
    src/Packet.cxx
#    src/TimeHistory.cxx
)

#add_library(${This} STATIC ${Sources} ${Headers})
#add_subdirectory(test)

add_executable(main ${Sources} ${Headers})
target_link_libraries(main)


